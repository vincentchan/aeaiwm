package com.agileai.wm.module.system.service;

import com.agileai.hotweb.bizmoduler.core.TreeSelectServiceImpl;
import com.agileai.wm.cxmodule.SecurityRoleTreeSelect;

public class SecurityRoleTreeSelectImpl
        extends TreeSelectServiceImpl
        implements SecurityRoleTreeSelect {
    public SecurityRoleTreeSelectImpl() {
        super();
    }
}
