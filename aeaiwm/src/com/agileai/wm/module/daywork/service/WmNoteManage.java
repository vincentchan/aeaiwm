package com.agileai.wm.module.daywork.service;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.StandardService;

public interface WmNoteManage
        extends StandardService {
	void changeCurrentSort(String nodeId, boolean b);
	DataRow queryCurrentRecord(DataParam queryParam);
}
