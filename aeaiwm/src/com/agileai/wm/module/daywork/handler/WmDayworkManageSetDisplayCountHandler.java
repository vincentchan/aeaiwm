package com.agileai.wm.module.daywork.handler;


import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.controller.core.StandardEditHandler;
import com.agileai.hotweb.domain.core.User;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.RedirectRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.wm.module.daywork.service.WmDayworkManage;
import com.agileai.wm.module.daywork.service.WmNoteManage;

public class WmDayworkManageSetDisplayCountHandler
        extends StandardEditHandler {
    public WmDayworkManageSetDisplayCountHandler() {
        super();
        this.listHandlerClass = WmDayworkManageListHandler.class;
        this.serviceId = buildServiceId(WmDayworkManage.class);
    }
    public ViewRenderer prepareDisplay(DataParam param) {
		String operaType = param.get(OperaType.KEY);
		if (isReqRecordOperaType(operaType)){
			DataRow record = getService().getRecord(param);
			this.setAttributes(record);	
		}
		this.setOperaType(operaType);
		User user = (User) this.getUser();
		param.put("USER_ID",user.getUserId());
		DataRow userRecord=getService().getUserRecord(param);
		int DISPLAY_COUNT =userRecord.getInt("DISPLAY_COUNT");
		setAttribute("DISPLAY_COUNT", DISPLAY_COUNT);
		processPageAttributes(param);
		return new LocalRenderer(getPage());
	}
    protected void processPageAttributes(DataParam param) {}
    public ViewRenderer doSaveAction(DataParam param){
    	if(param.get("daywork_note").equals("0")){
    		String operateType = param.get(OperaType.KEY);
    		if (OperaType.CREATE.equals(operateType)){
    			getService().createRecord(param);	
    		}
    		else if (OperaType.UPDATE.equals(operateType)){
    			getService().updateRecord(param);	
    		}
    	}else if (param.get("daywork_note").equals("1")){
    		String operateType = param.get(OperaType.KEY);
    		WmNoteManage wmNoteManage = lookupService(WmNoteManage.class);
    		if (OperaType.CREATE.equals(operateType)){
    			wmNoteManage.createRecord(param);	
    		}
    		else if (OperaType.UPDATE.equals(operateType)){
    			wmNoteManage.updateRecord(param);	
    		}
		}
    	
		return new RedirectRenderer(getHandlerURL(listHandlerClass));
	}
   /* @PageAction
   	public ViewRenderer saveDisplayCount(DataParam param) {
    	User user = (User) this.getUser();
		param.put("USER_ID",user.getUserId());
		getService().updateUserRecord(param);
		return prepareDisplay(param);
   	}*/
    protected WmDayworkManage getService() {
        return (WmDayworkManage) this.lookupService(this.getServiceId());
    }
   
}
